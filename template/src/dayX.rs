pub fn part_one(_pattern: &Vec<&str>) -> i32 {
    return 0;
}

pub fn part_two(_pattern: &Vec<&str>) -> i32 {
    return 0;
}

#[cfg(test)]
mod tests {
    use crate::dayX;

    #[test]
    fn test_part_one() {
        assert_eq!(dayX::part_one(&vec![]), 0);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(dayX::part_two(&vec![]), 0);
    }
}
