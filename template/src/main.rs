mod dayX;

fn main() {
    let input = vec![];

    println!("{}", dayX::part_one(&input));
    println!("{}", dayX::part_two(&input));
}
