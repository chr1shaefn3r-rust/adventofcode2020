pub fn highest_seat_id(pattern: &Vec<&str>) -> usize {
    let seat_positions: Vec<SeatPosition> = pattern.iter().map(seat_position).collect();
    let seat_ids: Vec<usize> = seat_positions.iter().map(seat_id).collect();
    let mut result: usize = 0;
    for seat_id in seat_ids.iter() {
        result = result.max(*seat_id);
    }
    return result;
}

pub fn part_missing_seat(pattern: &Vec<&str>) -> usize {
    let seat_positions: Vec<SeatPosition> = pattern.iter().map(seat_position).collect();
    let seat_ids: Vec<usize> = seat_positions.iter().map(seat_id).collect();
    let mut missing_seat_id: usize = 0;
    for row in 0..127 {
        for column in 0..7 {
            let seat_position = &SeatPosition{ row: row, column: column};
            let seat_id = seat_id(seat_position);
            if !seat_positions.contains(seat_position) && seat_ids.contains(&(seat_id+1)) && seat_ids.contains(&(seat_id-1)){
                missing_seat_id = seat_id;
            }
        }
    }
    return missing_seat_id;
}

pub fn seat_id(seat_position: &SeatPosition) -> usize {
    seat_position.row * 8 + seat_position.column
}

pub fn seat_position(seat_specifier: &&str) -> SeatPosition {
    let mut lower_boundary_row = 0;
    let mut upper_boundary_row = 127;
    let mut count_fb_s = 0;
    let mut row = 0;
    let mut lower_boundary_column = 0;
    let mut upper_boundary_column = 7;
    let mut count_lr_s = 0;
    let mut column = 0;
    for identifier in seat_specifier.chars() {
        if identifier == 'B' {
            count_fb_s += 1;
            if count_fb_s == 7 {
                row = upper_boundary_row;
            } else {
                lower_boundary_row = lower_boundary_row + (upper_boundary_row - lower_boundary_row) / 2;
                lower_boundary_row += 1;
            }
        } else if identifier == 'F' {
            count_fb_s += 1;
            if count_fb_s == 7 {
                row = lower_boundary_row;
            } else {
                upper_boundary_row = lower_boundary_row + (upper_boundary_row - lower_boundary_row) / 2;
            }
        } else if identifier == 'R' {
            count_lr_s += 1;
            if count_lr_s == 3 {
                column = upper_boundary_column;
            } else {
                lower_boundary_column = lower_boundary_column + (upper_boundary_column - lower_boundary_column) / 2;
                lower_boundary_column += 1;
            }
        } else if identifier == 'L' {
            count_lr_s += 1;
            if count_lr_s == 3 {
                column = lower_boundary_column;
            } else {
                upper_boundary_column = lower_boundary_column + (upper_boundary_column - lower_boundary_column) / 2;
            }
        }
    }
    SeatPosition{ row: row, column: column}
}

#[derive(PartialEq, Debug)]
pub struct SeatPosition {
    pub row: usize,
    pub column: usize,
}

#[cfg(test)]
mod tests {
    use crate::day5;

    #[test]
    fn test_seat_position() {
        assert_eq!(
            day5::seat_position(&"FBFBBFFRLR"),
            day5::SeatPosition{ row: 44, column: 5}
        );
    }
    #[test]
    fn test_highest_seat_id() {
        assert_eq!(
            day5::highest_seat_id(&vec!["BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"]),
            820
        );
    }
}
