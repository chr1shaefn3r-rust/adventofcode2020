pub fn find_two(numbers: &Vec<i32>) -> i32 {
    let mut summand_one = numbers[0];
    let mut summand_two = numbers[1];
    for search_one in numbers.iter() {
        for search_two in numbers.iter() {
            if search_one + search_two == 2020 {
                summand_one = *search_one;
                summand_two = *search_two;
            }
        }
    }
    return summand_one * summand_two;
}

pub fn find_three(numbers: &Vec<i32>) -> i32 {
    let mut summand_one = numbers[0];
    let mut summand_two = numbers[1];
    let mut summand_three = numbers[2];
    for search_one in numbers.iter() {
        for search_two in numbers.iter() {
            for search_three in numbers.iter() {
                if search_one + search_two + search_three == 2020 {
                    summand_one = *search_one;
                    summand_two = *search_two;
                    summand_three = *search_three;
                }
            }
        }
    }
    return summand_one * summand_two * summand_three;
}

#[cfg(test)]
mod tests {
    use crate::day1;

    #[test]
    fn test_find_two_example() {
        assert_eq!(day1::find_two(&vec![1721, 979, 366, 299, 675, 1456]), 514579);
    }

    #[test]
    fn test_find_three_example() {
        assert_eq!(
            day1::find_three(&vec![1721, 979, 366, 299, 675, 1456]),
            241861950
        );
    }
}
