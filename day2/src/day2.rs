use regex::Regex;

pub fn valid_passwords(policies: &Vec<&str>) -> i32 {
    let mut result = 0;
    let re = Regex::new(r"(\d+)-(\d+) (\w{1}): (\w+)").unwrap();
    for policy in policies.iter() {
        let caps = re.captures(policy).unwrap();

        let min = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
        let max = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
        let character = caps.get(3).unwrap().as_str().parse::<char>().unwrap();
        let password = caps.get(4).unwrap().as_str();

        let v: Vec<&str> = password.matches(character).collect();
        let count = v.len();

        if count >= min && count <= max {
            result += 1;
        }
    }
    return result;
}

pub fn valid_passwords_two(policies: &Vec<&str>) -> i32 {
    let mut result = 0;
    let re = Regex::new(r"(\d+)-(\d+) (\w{1}): (\w+)").unwrap();
    for policy in policies.iter() {
        let caps = re.captures(policy).unwrap();

        let first_position = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
        let second_position = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
        let character = caps.get(3).unwrap().as_str().parse::<char>().unwrap();
        let password = caps.get(4).unwrap().as_str();

        let characters: Vec<char> = password.chars().collect();
        let first_character: char = characters[first_position-1];
        let second_character = characters[second_position-1];

        if (first_character == character) ^ (second_character == character) {
            result += 1;
        }
    }
    return result;
}

#[cfg(test)]
mod tests {
    use crate::day2;

    #[test]
    fn test_valid_passwords_example() {
        assert_eq!(
            day2::valid_passwords(&vec!["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc", "6-13 f: mfswqfrqffrvfvf"]),
            3
        );
    }

    #[test]
    fn test_valid_passwords_two_example() {
        assert_eq!(
            day2::valid_passwords_two(&vec!["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]),
            1
        );
    }
}
