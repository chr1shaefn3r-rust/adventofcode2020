#!/bin/sh
DAY=$1
echo Creating new folder for day$DAY
cp -r template/ day$DAY
cd day$DAY
pwd
grep -RIl 'dayX' | xargs sed -i "s/dayX/day$DAY/g"
grep -RIl 'DayX' | xargs sed -i "s/DayX/Day$DAY/g"
grep -RIl 'day/X' | xargs sed -i "s/day\/X/day\/$DAY/g"
mv src/dayX.rs src/day$DAY.rs
cargo test
cargo run

