mod day3;

fn main() {
    let input = vec![
        "...#..............#.#....#..#..",
        "...#..#..#..............#..#...",
        "....#.#.......#............#...",
        "..##.....##.........#........##",
        "...#...........#...##.#...#.##.",
        "..#.#...#....#.....#........#..",
        "....##.###.....#..#.......#....",
        ".#..##...#.....#......#..#.....",
        "............##.#...#.#.....#.#.",
        "..........#....#....#.#...#...#",
        "..##....#.#.#......#.........#.",
        "#.#.........#..............##..",
        "....##.##......................",
        "....##..#...........#..........",
        "..#..#.#........##....#......#.",
        "..............#..#....#.....#..",
        ".............#...#.....#...#...",
        ".#...........#..........#...#..",
        ".#......#.......#......#.......",
        "#..#.............#..#....##.###",
        "........#.#...........##.#...#.",
        "......#..#.....##......#.......",
        ".....#.....#....#..............",
        "#...##.#......#......#...#.....",
        "...........................#...",
        "...#....................#.....#",
        "..#.....#...#.....##.....#.....",
        "....................#......#..#",
        ".......#.....##......##....#...",
        "#........##...#.....##..#...#..",
        "........#..#.#......#..###..#.#",
        "##.....#.............#.#....#..",
        "..#.................#....######",
        ".#.#..#.....#.#..........#.#...",
        ".........#....#...#............",
        "........#..#.....#.............",
        "............#.#.............##.",
        "...#....#..#......#............",
        ".##....#.....#...#.#...........",
        "..#..............#...........##",
        ".....#.#.##...#................",
        "..........#..#.#..........##..#",
        "..#....#...#...#.....######....",
        "....#.#..#........#....#.###...",
        ".......................#.......",
        "..#.....#.##................#..",
        ".....#......#..#.....#........#",
        ".#...###.......#.#.........#..#",
        "............#..................",
        "..#.........##.........##......",
        "#...........#.#.......###.#....",
        ".#...#.....#.........###.....#.",
        ".#............#........#..#....",
        "...##.#......##................",
        "........#...#...#...#..........",
        ".......#.##......##.#..........",
        "....##.......#..#....#....#....",
        "......#.........###........#...",
        "#....#....####....##......#....",
        "......................#....#.#.",
        "...#.#.#....#.#...#...#......#.",
        "......#.....##.#...........###.",
        "#........#.........##......#.#.",
        "....##.....#.....#..#..........",
        "......#...#...#.........#...##.",
        "..#........#..................#",
        ".........#..##.#..#..#...#.#..#",
        ".....#.....#...#.....###.....##",
        ".............#....#...#........",
        "..........#.#.#...#..#...#....#",
        "#...............##.......#.....",
        "#...#.............#..#...#....#",
        "..#...#...##...##...#..#.......",
        "..#..#........#.#...........#..",
        ".....#.....#..................#",
        "....#....##....###..###...##...",
        "..#......###.........##....#.##",
        ".......#.##...#.......#..#.....",
        "...#.#.#.#.....##..#..#........",
        "................##....#.#......",
        "..#...#...#...#.....##.#...#..#",
        "..#..#.....#..##....#....#.....",
        ".###...#......#........#.....##",
        "##......#..#........#..........",
        "....#...#..#....##..#......####",
        ".#.....##....#..........#......",
        ".#...#....#.........#...#....#.",
        ".....#..#.#..#......#..##....#.",
        "...#.##...#...#........#......#",
        ".#..#...#.#..#.........#...#...",
        "#....#......##.....#.......#...",
        "..##............##..#.#.#...#..",
        "##.......#.......##............",
        "#......#.##........#...#...#...",
        ".#.#.......##.........#..#.#...",
        ".............##.#........#.....",
        ".#..#...###...#..#.............",
        ".....#...#..#....#.......#.....",
        "#.#.........#.#.#...#...#.#....",
        ".....#.......#.##.##...#....#..",
        ".#.##..#.....#...#.#.#.#.#..#..",
        "..........#...................#",
        ".....#.#.#...##.........#..#..#",
        ".#..#....##......#...#.........",
        ".##......#......#...#........#.",
        ".....##.#......#............#.#",
        ".#.....##..#...........##......",
        ".....#......#.......##....#....",
        "..#..##..........#.#..........#",
        "#.#.......##..#..##.#....#.....",
        ".......#..#.#.......##......#.#",
        "....#...##...#..............#..",
        ".....#.........#......#...##...",
        "#.........#........##..#.....#.",
        ".#.#..#.....##.......#......#..",
        "........#..#....#.....###..#...",
        "#.#..#.#..........#............",
        "..#......##..#....#.........#..",
        "#..............................",
        ".......#............#..#..#.#..",
        ".#.....#.#....#..#.##.#........",
        ".......#.###.#........##.#..#..",
        "..............#....#.....##.#..",
        "#..............#....#.###......",
        ".#..#..#...###............#...#",
        ".#.##...#....#..#...#...#......",
        "......##..#..#......#..#....#..",
        ".........#.......##............",
        "...........##...#..#....####...",
        ".#..................#..........",
        "#...#..#..................#....",
        "..............#.....##.....#...",
        "..#.#..#...##..#.....#.....#..#",
        "....#....#.#.........#.....#...",
        ".#.......#...#....#...#.#..#..#",
        "#.........##.....##.......#...#",
        "#..#............#....#........#",
        "..........##...#......#....#...",
        ".......#..##...............#...",
        "#............#.#.#.....#.......",
        ".#........##...#...............",
        "..##....#.....#..#.##.#......#.",
        ".#...#.............#...#.....#.",
        "...##....#.......#......#.#..#.",
        "#......................#..#.##.",
        "...#..........#..#.........#...",
        "..#......#.......#.#....#......",
        "....#............#...#......#..",
        ".....#..#..##...#...#.........#",
        ".....#......#....#....#........",
        ".............#..#..........#...",
        "....#..............#.....#.#...",
        "....#.................#.#...#.#",
        ".........#.#...........###.#.##",
        "#...........#..##.#....#.##.#..",
        "#.#.....#......................",
        "##.#.........#....##.#.#..#.#..",
        "#..........#.#.#.#.#..#..##..#.",
        "..#...#..###.........#......#..",
        ".....#......#..#.#............#",
        "...........#...#.#.#.###....#..",
        "#....#..#.......##.#.......##..",
        "..............#.....##.#.......",
        ".#.....#.#..#.........#.#.#..#.",
        "..#..#..#..#................#..",
        "...........#..#.#...#.........#",
        ".#..#..#...#........#...#.#..#.",
        "...#.#..#......#..#............",
        "........#......##.....#....#...",
        "#...#......##.#.#..............",
        ".#........................#....",
        "#.#.....#.##.....#..#.#........",
        "#..........##.#.......#....#..#",
        "#...#..#..#.....#....#....#....",
        "#...........#..#.#....##.##....",
        "##......#..#........#.......##.",
        "#........#..#...#..........#...",
        "...#...#......##....#.#........",
        "...##..#..#.##....#...#........",
        "#.#..#....#...#........#.......",
        "..........#.......#..........#.",
        "......##...#....###...#....#...",
        "........#..#.....#......#......",
        "....#.........##...#..##......#",
        "....#...........#.#..#.#.#.#..#",
        "..#......#..#......#........#.#",
        "#..#....#.....#.............#..",
        "............................#..",
        "#...#.#.....#...#....#....#....",
        "........#...#...#...#...#......",
        ".###........#....#.##.....##.#.",
        ".........#.....#..........#....",
        ".#.........#....##.#.....#.....",
        "#..#....................##.#...",
        "..##.#.............#....#.#....",
        "..#.#........#............##.#.",
        "#........#...##.....#...#.....#",
        ".........#.#..........#....#..#",
        "...###.#..#.#......#.#.....#...",
        "......#.....#..#...#........#..",
        ".......#...#.....#....#....#..#",
        ".#.#........#......##.......#..",
        "#.................###..........",
        "#........#.#..#....#..#........",
        "..##....#.#...##...#...##....#.",
        "...#.#......##...#.....#..#....",
        "#..#........#...###....#.......",
        "##.#....#..#.#..........#......",
        "....#...###...#.....#........#.",
        "..#.#........#....##.#.........",
        "......##.##.................##.",
        ".#....##...#.#..#.#............",
        ".#...###........#......#.......",
        "##..#.#......#..#..#...#.......",
        ".......##..#....#........#....#",
        "......#..........#.............",
        "....##..##..#......#.#.........",
        ".....#....................##...",
        "...###.....#.....#...#.#.##.#..",
        "....#.#..#.......#..#......##..",
        ".......#.#..#.##.#...#......#..",
        "...#.#....#.#...#..##...#...#..",
        "#.##...#....#..#.............#.",
        "...#...#...#.......#..........#",
        ".#..#.............#..##.#......",
        "....#.......#..............#.#.",
        "..................#..#.....##.#",
        ".#...#..#......#..........#...#",
        "..#.#.....#..#....#....#####.#.",
        ".......###.......#....#....#...",
        "......#.#........#...#.........",
        "......#..#.#.#....#.#.#....##..",
        ".#...#.#...##.#......#.........",
        "#....#..##....#.#.......#....#.",
        "..##.#.....#.....#.........#...",
        "......#......#....#....#.....#.",
        "...##.....#....#......#......#.",
        "......#......##............#.#.",
        ".##.#.......#....#...#....#....",
        "....#..#..#...##.......#..#....",
        "....#....#...#.#........#..#...",
        "....#.....#..........#..#......",
        "....#....#...#.....#..##.....#.",
        "##...#..##......#....##..#..#..",
        ".....##.##..............##.....",
        "#.#....#.##..#....#...##.......",
        "..#.....##.....#.....######...#",
        "...#.....#.#.#......#......##.#",
        "...........##.............#....",
        "...##......#..#......#...#.....",
        "....#.##......#..#....#.#..#...",
        ".#..#....#...#..#.....##.......",
        ".....#..#.................#..#.",
        "................#..#...#......#",
        "...##....#.....#..#....##......",
        "....##...............##...#....",
        "......#..........#..##.........",
        ".......###.......#.........#..#",
        "......................#....#.#.",
        "#.#.....#...##............#....",
        "........#......##......#.....#.",
        "...#....#....#.#..##.#..#.#.#..",
        "..#.#....#.##...#..#.....#.#...",
        "............#....#..#.......#..",
        "#...#...#.#......#...##.....#.#",
        "......#....#....#.......#......",
        "....#.......#..........#....#..",
        "........#####........#....#....",
        "......#....##..............#.#.",
        "....#....#.......#.......#.....",
        ".##.#....##....#...............",
        "#.....##........#..#.#...#.#...",
        "...#......##....#..............",
        ".#.....#.....#.......##....##..",
        "#....#..........#.#..#.........",
        "......##..........##.......#...",
        ".##......##.....#.#....#......#",
        "....#......................#...",
        ".#...........###........#...#..",
        "#.#..#..#..#...##.#....#.#..#..",
        "...##...........#.#..........#.",
        "......#.#..#....#....#.........",
        "....#....#.#......#.........##.",
        ".#..#...#...##....#...#......#.",
        "#.#......#...#.#.#...........#.",
        "##.....#..........##....##..##.",
        "...#.#.....#..##........#......",
        "..#........##........#..#......",
        ".......#...............##..#...",
        ".......#.#....#..###...........",
        ".............#........#...#....",
        "#.................#......#..#..",
        "...#....#..#..............#...#",
        ".............#....##....#..##..",
        "#........#..........##...##...#",
        "............#....#.....#.#....#",
        ".....#..............##..#...#..",
        "..#....#......###....#.#...##..",
        "....##......#.....#....#.......",
        ".....#...............#.....#...",
        ".#.....#.....#..............#..",
        "#................#..#..........",
        ".##....#....#.....#............",
        "#.####...#..#..#....#..........",
        "..##........##.....##......#..#",
        "......#.....#...##.........##..",
        "....##..#.....#.#.........#...#",
        ".....##..#....#....#.#...#..#..",
        "...#............#...........#..",
        ".......#.#..#.#.#..#........#.#",
        "....#.#........#.#.#..#...#...#",
        "..#....#....#..#......#........",
        ".#...........................#.",
        ".#..#....####........##......#.",
        ".#.....#..#.#.................#",
        ".#..#...........#...#....#...#.",
        "......##..#........#..#....#...",
        "..#.............#....#........#",
        "#.#..........#.##.......#.#..#.",
        "..#....#...#...............#...",
        "..............#..........#..#..",
        "..#.....#.#.....#...#...#..#...",
        ".........#...###.#...#........#",
    ];

    println!("{}", day3::how_many_trees(&input));
    println!("{}", day3::how_many_trees_multi_slope(&input));
}
