pub fn how_many_trees(pattern: &Vec<&str>) -> i32 {
    return how_many_trees_one_slope(pattern, 3, 1);
}

fn how_many_trees_one_slope(pattern: &Vec<&str>, step_x: usize, step_y: usize) -> i32 {
    let mut position_x = 0;
    let mut position_y = 0;
    let mut result = 0;
    let pattern_height = pattern.len();
    let pattern_width = pattern[0].chars().count();

    loop {
        let line: Vec<char> = pattern[position_y].chars().collect();
        let character: char = line[position_x % pattern_width];

        if character == '#' {
            result += 1;
        }
        position_x += step_x;
        position_y += step_y;

        if (position_y + step_y) > pattern_height {
            return result;
        }
    }
}

pub fn how_many_trees_multi_slope(pattern: &Vec<&str>) -> i32 {
    let first_slope = how_many_trees_one_slope(pattern, 1, 1);
    let second_slope = how_many_trees_one_slope(pattern, 3, 1);
    let third_slope = how_many_trees_one_slope(pattern, 5, 1);
    let fourth_slope = how_many_trees_one_slope(pattern, 7, 1);
    let fifth_slope = how_many_trees_one_slope(pattern, 1, 2);

    return first_slope * second_slope * third_slope * fourth_slope * fifth_slope;
}

#[cfg(test)]
mod tests {
    use crate::day3;

    #[test]
    fn test_how_many_trees() {
        assert_eq!(
            day3::how_many_trees(&vec![
                "..##.......",
                "#...#...#..",
                ".#....#..#.",
                "..#.#...#.#",
                ".#...##..#.",
                "..#.##.....",
                ".#.#.#....#",
                ".#........#",
                "#.##...#...",
                "#...##....#",
                ".#..#...#.#"
            ]),
            7
        );
    }

    #[test]
    fn test_how_many_trees_multislope() {
        assert_eq!(
            day3::how_many_trees_multi_slope(&vec![
                "..##.......",
                "#...#...#..",
                ".#....#..#.",
                "..#.#...#.#",
                ".#...##..#.",
                "..#.##.....",
                ".#.#.#....#",
                ".#........#",
                "#.##...#...",
                "#...##....#",
                ".#..#...#.#"
            ]),
            336
        );
    }
}
