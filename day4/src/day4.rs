use regex::Regex;

/*
    byr (Birth Year)
    iyr (Issue Year)
    eyr (Expiration Year)
    hgt (Height)
    hcl (Hair Color)
    ecl (Eye Color)
    pid (Passport ID)
    cid (Country ID)
*/
pub fn valid_passports(pattern: &Vec<&str>) -> i32 {
    let mut result = 0;
    let regex_byr = Regex::new(r"byr:").unwrap();
    let regex_iyr = Regex::new(r"iyr:").unwrap();
    let regex_eyr = Regex::new(r"eyr:").unwrap();
    let regex_hgt = Regex::new(r"hgt:").unwrap();
    let regex_hcl = Regex::new(r"hcl:").unwrap();
    let regex_ecl = Regex::new(r"ecl:").unwrap();
    let regex_pid = Regex::new(r"pid:").unwrap();

    let mut has_byr = false;
    let mut has_iyr = false;
    let mut has_eyr = false;
    let mut has_hgt = false;
    let mut has_hcl = false;
    let mut has_ecl = false;
    let mut has_pid = false;
    for line in pattern.iter() {
        if line.bytes().count() == 0 {
            if has_byr && has_iyr && has_eyr && has_hgt && has_hcl && has_ecl && has_pid {
                result += 1;
            }
            has_byr = false;
            has_iyr = false;
            has_eyr = false;
            has_hgt = false;
            has_hcl = false;
            has_ecl = false;
            has_pid = false;
        }
        for token in line.split_whitespace() {
            if regex_byr.is_match(token) {
                has_byr = true;
            } else if regex_iyr.is_match(token) {
                has_iyr = true;
            } else if regex_eyr.is_match(token) {
                has_eyr = true;
            } else if regex_hgt.is_match(token) {
                has_hgt = true;
            } else if regex_hcl.is_match(token) {
                has_hcl = true;
            } else if regex_ecl.is_match(token) {
                has_ecl = true;
            } else if regex_pid.is_match(token) {
                has_pid = true;
            };
        }
    }

    return result;
}

/*
    byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
    If cm, the number must be at least 150 and at most 193.
    If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.
*/
pub fn valid_passports_strict(pattern: &Vec<&str>) -> i32 {
    let mut result = 0;
    let regex_byr = Regex::new(r"byr:(\d{4})$").unwrap();
    let regex_iyr = Regex::new(r"iyr:(\d{4})$").unwrap();
    let regex_eyr = Regex::new(r"eyr:(\d{4})$").unwrap();
    let regex_hgt_cm = Regex::new(r"hgt:(\d{3})cm").unwrap();
    let regex_hgt_in = Regex::new(r"hgt:(\d{2})in").unwrap();
    let regex_hcl = Regex::new(r"hcl:#[a-f0-9]{6}").unwrap();
    let regex_ecl = Regex::new(r"ecl:(amb|blu|brn|gry|grn|hzl|oth)").unwrap();
    let regex_pid = Regex::new(r"pid:\d{9}$").unwrap();

    let mut has_byr = false;
    let mut has_iyr = false;
    let mut has_eyr = false;
    let mut has_hgt = false;
    let mut has_hcl = false;
    let mut has_ecl = false;
    let mut has_pid = false;
    for line in pattern.iter() {
        if line.bytes().count() == 0 {
            if has_byr && has_iyr && has_eyr && has_hgt && has_hcl && has_ecl && has_pid {
                result += 1;
            }
            has_byr = false;
            has_iyr = false;
            has_eyr = false;
            has_hgt = false;
            has_hcl = false;
            has_ecl = false;
            has_pid = false;
        }
        for token in line.split_whitespace() {
            if regex_byr.is_match(token) {
                // byr (Birth Year) - four digits; at least 1920 and at most 2002.
                let captures = regex_byr.captures(token).unwrap();
                let byr = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
                if byr >= 1920 && byr <= 2002 {
                    has_byr = true;
                }
            } else if regex_iyr.is_match(token) {
                // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
                let captures = regex_iyr.captures(token).unwrap();
                let iyr = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
                if iyr >= 2010 && iyr <= 2020 {
                    has_iyr = true;
                }
            } else if regex_eyr.is_match(token) {
                // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
                let captures = regex_eyr.captures(token).unwrap();
                let eyr = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
                if eyr >= 2020 && eyr <= 2030 {
                    has_eyr = true;
                }
            } else if regex_hgt_cm.is_match(token) {
                // hgt (Height) - a number followed by either cm or in:
                // If cm, the number must be at least 150 and at most 193.
                let captures = regex_hgt_cm.captures(token).unwrap();
                let hgt_cm = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
                if hgt_cm >= 150 && hgt_cm <= 193 {
                    has_hgt = true;
                }
            } else if regex_hgt_in.is_match(token) {
                // hgt (Height) - a number followed by either cm or in:
                // If in, the number must be at least 59 and at most 76.
                let captures = regex_hgt_in.captures(token).unwrap();
                let hgt_in = captures.get(1).unwrap().as_str().parse::<usize>().unwrap();
                if hgt_in >= 59 && hgt_in <= 76 {
                    has_hgt = true;
                }
            } else if regex_hcl.is_match(token) {
                has_hcl = true;
            } else if regex_ecl.is_match(token) {
                has_ecl = true;
            } else if regex_pid.is_match(token) {
                println!("{}", token);
                has_pid = true;
            };
        }
    }

    return result;
}

#[cfg(test)]
mod tests {
    use crate::day4;

    #[test]
    fn test_valid_passports() {
        assert_eq!(
            day4::valid_passports(&vec![
                "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
                "byr:1937 iyr:2017 cid:147 hgt:183cm",
                "",
                "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
                "hcl:#cfa07d byr:1929",
                "",
                "hcl:#ae17e1 iyr:2013",
                "eyr:2024",
                "ecl:brn pid:760753108 byr:1931",
                "hgt:179cm",
                "",
                "hcl:#cfa07d eyr:2025 pid:166559648",
                "iyr:2011 ecl:brn hgt:59in",
                ""
            ]),
            2
        );
    }

    #[test]
    fn test_valid_passports_optional_ci() {
        assert_eq!(
            day4::valid_passports(&vec![
                "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
                "byr:1937 iyr:2017 hgt:183cm",
                "cid:147",
                "",
            ]),
            1
        );
    }

    #[test]
    fn test_valid_passports_strict() {
        assert_eq!(
            day4::valid_passports_strict(&vec![
                "eyr:1972 cid:100",
                "hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926",
                "",
                "iyr:2019",
                "hcl:#602927 eyr:1967 hgt:170cm",
                "ecl:grn pid:012533040 byr:1946",
                "",
                "hcl:dab227 iyr:2012",
                "ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277",
                "",
                "hgt:59cm ecl:zzz",
                "eyr:2038 hcl:74454a iyr:2023",
                "pid:3556412378 byr:2007",
                "",
                "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980",
                "hcl:#623a2f",
                "",
                "eyr:2029 ecl:blu cid:129 byr:1989",
                "iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm",
                "",
                "hcl:#888785",
                "hgt:164cm byr:2001 iyr:2015 cid:88",
                "pid:545766238 ecl:hzl",
                "eyr:2022",
                "",
                "iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719",
                ""
            ]),
            4
        );
    }

    #[test]
    fn test_valid_passports_strict_wrong_value_range() {
        assert_eq!(
            day4::valid_passports_strict(&vec![
                "pid:087499704 hgt:58in ecl:grn iyr:2012 eyr:2030 byr:1980",
                "hcl:#623a2f",
                "",
                "eyr:2029 ecl:blu cid:129 byr:1989",
                "iyr:2014 pid:896056539 hcl:#a97842 hgt:200cm",
                "",
                "hcl:#888785",
                "hgt:164cm byr:2001 iyr:2015 cid:88",
                "pid:545766238 ecl:hzl",
                "eyr:2035",
                "",
                "iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1901 eyr:2021 pid:093154719",
                "",
                "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:2003",
                "hcl:#623a2f",
                "",
                "pid:087499704 hgt:190in ecl:grn iyr:2012 eyr:2030 byr:1980",
                "hcl:#623a2f",
                "",
                "pid:0874997045 hgt:190in ecl:grn iyr:2012 eyr:2030 byr:1980",
                "hcl:#623a2f",
                ""
            ]),
            0
        );
    }
}
