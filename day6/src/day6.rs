use std::collections::HashSet;

pub fn yes_anyone(pattern: &Vec<&str>) -> usize {
    let mut unique_answers: HashSet<char> = HashSet::new();
    let mut result = 0;
    for answers in pattern.iter() {
        if answers.chars().count() == 0 {
            result += unique_answers.len();
            unique_answers = HashSet::new();
        }
        for answer in answers.chars() {
            unique_answers.insert(answer);
        }
    }
    return result;
}

pub fn yes_everyone(pattern: &Vec<&str>) -> usize {
    let mut unique_answers: HashSet<char> = HashSet::new();
    let mut group_answers: HashSet<char> = HashSet::new();
    let mut result = 0;
    for answers in pattern.iter() {
        if answers.chars().count() == 0 {
            result += unique_answers.len();
            unique_answers = HashSet::new();
            group_answers = HashSet::new();
        }
        if group_answers.len() == 0 {
            for answer in answers.chars() {
                group_answers.insert(answer);
            }
            let mut temp_unique_anwers = HashSet::new();
            for answer in answers.chars() {
                if group_answers.contains(&answer) {
                    temp_unique_anwers.insert(answer);
                }
            }
            unique_answers = temp_unique_anwers;
        }
        let mut temp_unique_anwers = HashSet::new();
        for answer in answers.chars() {
            if unique_answers.contains(&answer) {
                temp_unique_anwers.insert(answer);
            }
        }
        unique_answers = temp_unique_anwers;
    }
    return result;
}

#[cfg(test)]
mod tests {
    use crate::day6;

    #[test]
    fn test_yes_anyone() {
        assert_eq!(
            day6::yes_anyone(&vec![
                "abc", "", "a", "b", "c", "", "ab", "ac", "", "a", "a", "a", "a", "", "b", ""
            ]),
            11
        );
    }

    #[test]
    fn test_yes_everyone() {
        // assert_eq!(
        //     day6::yes_everyone(&vec![
        //         "fekdcbayqxnwvh",
        //         "fycwqktvxandeb",
        //         "kqbafvcxyewrdn",
        //         "akwqcvenxfydbs",
        //         "ewbaxdcvnkyfq",
        //         "", //13
        //         "timjneyhbvxkfagdpzrous",
        //         "gsumijvxoheptbafnkyzrd",
        //         "yxtbnupramvdezhkfojsig",
        //         "soaruhxnpiemjvytzbfdkg",
        //         "vfanlgjoiskzmubtxhceyprd",
        //         "", //22
        //         "dhgabzfspkltq",
        //         "pflzsbtaxhqdkg",
        //         "kzpbfgasthldq",
        //         "", //13
        //         "xeu",
        //         "uoe",
        //         "tnpeox",
        //         "e",
        //         "vrdzwglecbk",
        //         "", //1
        //         "nucwl",
        //         "hnmx",
        //         "fqv",
        //         "", //0
        //         "kfubtzpovrdlw",
        //         "vdjmblrztpkosfg",
        //         "kavpdbgfzreol",
        //         "qpdzfrvokibcy",
        //         "", //9
        //         "yhknrqpubeolzixtvsj",
        //         "tlysdjzovxhnuqreimk",
        //         "", //17
        //         "lho",
        //         "hol",
        //         "jfolhv",
        //         "", //3
        //     ]),
        //     (13 + 22 + 13 + 1 + 0 + 9 + 17 + 3)
        // );
        assert_eq!(day6::yes_everyone(&vec!["tbc", "tqbe", "tcub", "",]), 2);
        // assert_eq!(day6::yes_everyone(&vec!["oudzygfimh", "xvj", "",]), 0);
        // assert_eq!(day6::yes_everyone(&vec!["t", "p", "p", "o", "",]), 0);
        // assert_eq!(day6::yes_everyone(&vec!["iv", "os", "",]), 0);
        // assert_eq!(
        //     day6::yes_everyone(&vec![
        //         "ealugrzsvhpxfmjdbqknoy",
        //         "doyhvplkjqarnbeutxsgfmz",
        //         "uhvxjaklmzoyqdbegrpfsn",
        //         "",
        //     ]),
        //     22
        // );
        // assert_eq!(
        //     day6::yes_everyone(&vec!["ejrmqwg", "merjguw", "jmreugw", "mjrgwtie", "",]),
        //     6
        // );
        // assert_eq!(
        //     day6::yes_everyone(&vec!["tdev", "vh", "wva", "va", "gzv", "",]),
        //     1
        // );
        // assert_eq!(day6::yes_everyone(&vec!["su", "s", "s", "s", "s", "",]), 1);
        // assert_eq!(day6::yes_everyone(&vec!["qmo", "oqm", "",]), 3);
        // assert_eq!(
        //     day6::yes_everyone(&vec!["xeu", "uoe", "tnpeox", "e", "vrdzwglecbk", "",]),
        //     1
        // );
        // assert_eq!(
        //     day6::yes_everyone(&vec![
        //         "abc", "", "a", "b", "c", "", "ab", "ac", "", "a", "a", "a", "a", "", "b", ""
        //     ]),
        //     6
        // );
        // assert_eq!(day6::yes_everyone(&vec!["ab", "ba", "",]), 2);
        // assert_eq!(
        //     day6::yes_everyone(&vec!["a", "b", "", "a", "b", "c", "",]),
        //     0
        // );
        // assert_eq!(day6::yes_everyone(&vec!["abc", "", "a", "b", "c", "",]), 3);
    }
}
